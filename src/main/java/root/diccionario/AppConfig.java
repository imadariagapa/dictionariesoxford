
package root.diccionario;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Contact;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.servers.Server;

/**
 * @author Ingrid_Madariaga
 */

@ApplicationPath("/api")
@OpenAPIDefinition(
        info = @Info(
                title = "Evaluación Final", 
                version = "1.0.0", 
                contact = @Contact(
                        name = "Ingrid Madariaga Parraguez", 
                        email = "ingrid.madariaga.parraguez@ciisa.cl", 
                        url = "")
                ), servers = {
                        @Server(
                                url = "/", 
                                description = "localhost")
                        }
)
public class AppConfig {
    
}
