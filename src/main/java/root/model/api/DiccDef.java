
package root.model.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import javax.naming.spi.DirStateFactory.Result;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "metadata",
    "results",
    "word"
})
public class DiccDef {
    
     @JsonProperty("id")
    public String id;
    @JsonProperty("metadata")
    public Metadata metadata;
    @JsonProperty("results")
    public List<Result> results = new ArrayList<Result>();
    @JsonProperty("word")
    public String word;
    @JsonIgnore
    private final Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public DiccDef withId(String id) {
        this.id = id;
        return this;
    }

    public DiccDef withMetadata(Metadata metadata) {
        this.metadata = metadata;
        return this;
    }

    public DiccDef withResults(List<Result> results) {
        this.results = results;
        return this;
    }

    public DiccDef withWord(String word) {
        this.word = word;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public DiccDef withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    
}
