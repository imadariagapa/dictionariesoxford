
package root.model.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.model.dao.exceptions.NonexistentEntityException;
import root.model.dao.exceptions.PreexistingEntityException;
import root.model.entities.Dictionariesoxford;

/**
 *
 * @author madar
 */
public class DictionariesoxfordDAO implements Serializable {

    public DictionariesoxfordDAO(EntityManagerFactory emf) {
        this.emf = Persistence.createEntityManagerFactory("Diccionario_PU");
    }
    private EntityManagerFactory emf = null;

    public DictionariesoxfordDAO() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Dictionariesoxford dictionariesoxford) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(dictionariesoxford);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDictionariesoxford(dictionariesoxford.getDicId()) != null) {
                throw new PreexistingEntityException("Dictionariesoxford " + dictionariesoxford + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Dictionariesoxford dictionariesoxford) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            dictionariesoxford = em.merge(dictionariesoxford);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = dictionariesoxford.getDicId();
                if (findDictionariesoxford(id) == null) {
                    throw new NonexistentEntityException("The dictionariesoxford with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Dictionariesoxford dictionariesoxford;
            try {
                dictionariesoxford = em.getReference(Dictionariesoxford.class, id);
                dictionariesoxford.getDicId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The dictionariesoxford with id " + id + " no longer exists.", enfe);
            }
            em.remove(dictionariesoxford);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Dictionariesoxford> findDictionariesoxfordEntities() {
        return findDictionariesoxfordEntities(true, -1, -1);
    }

    public List<Dictionariesoxford> findDictionariesoxfordEntities(int maxResults, int firstResult) {
        return findDictionariesoxfordEntities(false, maxResults, firstResult);
    }

    private List<Dictionariesoxford> findDictionariesoxfordEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Dictionariesoxford.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Dictionariesoxford findDictionariesoxford(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Dictionariesoxford.class, id);
        } finally {
            em.close();
        }
    }

    public int getDictionariesoxfordCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Dictionariesoxford> rt = cq.from(Dictionariesoxford.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<DictionariesoxfordDAO> findWord(String word) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Long getDicId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
