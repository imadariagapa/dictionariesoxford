/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model.domain;

/**
 *
 * @author madar
 */
public class MessageResponse {

    Long dicId;
    String object;
    String message;

    public Long getDicId() {
        return dicId;
    }

    public void setId(Long dicId) {
        this.dicId = dicId;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MessageResponse(Long dicId, String object, String message) {
        this.dicId = dicId;
        this.object = object;
        this.message = message;
    }
    
    
}
