/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model.domain;

import java.util.List;

/**
 *
 * @author madar
 */
public class ResultResponse {
    
    private Long dicId;
    private String dicPalabra;
    private List<String> dicDefinicion;

    public Long getDicId() {
        return dicId;
    }

    public void setDicId(Long dicId) {
        this.dicId = dicId;
    }

    public String getDicPalabra() {
        return dicPalabra;
    }

    public void setDicPalabra(String dicPalabra) {
        this.dicPalabra = dicPalabra;
    }

    public List<String> getDicDefiniciones() {
        return dicDefinicion;
    }

    public void setDicDefinicion(List<String> dicDefinicion) {
        this.dicDefinicion = dicDefinicion;
    }

    
}
