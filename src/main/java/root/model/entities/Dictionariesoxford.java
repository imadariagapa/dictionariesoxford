/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author madar
 */
@Entity
@Table(name = "dictionariesoxford")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Dictionariesoxford.findAll", query = "SELECT d FROM Dictionariesoxford d"),
    @NamedQuery(name = "Dictionariesoxford.findByDicId", query = "SELECT d FROM Dictionariesoxford d WHERE d.dicId = :dicId"),
    @NamedQuery(name = "Dictionariesoxford.findByDicPalabra", query = "SELECT d FROM Dictionariesoxford d WHERE d.dicPalabra = :dicPalabra"),
    @NamedQuery(name = "Dictionariesoxford.findByDicDefinicion", query = "SELECT d FROM Dictionariesoxford d WHERE d.dicDefinicion = :dicDefinicion")})
public class Dictionariesoxford implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "dic_id")
    private String dicId;
    @Size(max = 2147483647)
    @Column(name = "dic_palabra")
    private String dicPalabra;
    @Size(max = 2147483647)
    @Column(name = "dic_definicion")
    private String dicDefinicion;

    public Dictionariesoxford() {
    }

    public Dictionariesoxford(String dicId) {
        this.dicId = dicId;
    }

    public String getDicId() {
        return dicId;
    }

    public void setDicId(String dicId) {
        this.dicId = dicId;
    }

    public String getDicPalabra() {
        return dicPalabra;
    }

    public void setDicPalabra(String dicPalabra) {
        this.dicPalabra = dicPalabra;
    }

    public String getDicDefinicion() {
        return dicDefinicion;
    }

    public void setDicDefinicion(String dicDefinicion) {
        this.dicDefinicion = dicDefinicion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dicId != null ? dicId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dictionariesoxford)) {
            return false;
        }
        Dictionariesoxford other = (Dictionariesoxford) object;
        if ((this.dicId == null && other.dicId != null) || (this.dicId != null && !this.dicId.equals(other.dicId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.model.entities.Dictionariesoxford[ dicId=" + dicId + " ]";
    }
    
}
