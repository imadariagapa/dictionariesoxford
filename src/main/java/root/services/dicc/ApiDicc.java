
package root.services.dicc;

import java.io.IOException;
import java.util.logging.Logger;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import root.model.api.DiccDef;

import com.google.gson.*;

public class ApiDicc {
    
    private final Logger log = Logger.getLogger(DiccDef.class.getName());

    private final String prefixUrl = "https://od-api.oxforddictionaries.com/api/v2/entries/es/";
    private final String suffixUrl = "?fields=definitions&strictMatch=false";
    private OkHttpClient client = new OkHttpClient();
    private Gson gson = new Gson();

    public DiccDef getDefinitions(String word) throws IOException {

        //log.info("Environment::" + gson.toJson(System.getProperties()));
        String app_id = System.getProperty("OXFORD_APP_ID");
        String app_key = System.getProperty("OXFORD_APP_KEY");

        log.info("Request::app_id::" + app_id);
        log.info("Request::app_key::" + app_key);
        log.info("Request::Url" + prefixUrl + word + suffixUrl);

        Request request = new Request.Builder()
                .url(prefixUrl + word + suffixUrl)
                .get()
                .addHeader("accept", "application/json")
                .addHeader("app_id", "ddfb5e36")
                .addHeader("app_key", "877363d00c80d9147e5c7dc03f41999a")
                //.addHeader("app_if", app_id)
                //.addHeader("app_key", app_key)
                .build();

        Response response = client.newCall(request).execute();

        DiccDef body = gson.fromJson(response.body().string(), DiccDef.class);

        log.info("Response::" + body.toString());

        return body;
    }
    
}
