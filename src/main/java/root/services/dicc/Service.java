
package root.services.dicc;

import java.util.HashMap;

import java.util.List;
import java.util.logging.Logger;

import com.google.gson.Gson;
import root.model.entities.Dictionariesoxford;
import root.model.api.Entry;
import root.model.api.LexicalEntry;
import root.model.api.DiccDef;
import root.model.api.Result;
import root.model.api.Sense;
import root.model.dao.DictionariesoxfordDAO;
import root.model.dao.exceptions.FunctionalException;
import root.model.dao.exceptions.PreexistingEntityException;
import root.model.domain.HistorialResponse;
import root.model.domain.ResultResponse;
import java.util.ArrayList;

public class Service {

//    private Gson gson = new Gson();
//    private final Logger log = Logger.getLogger(this.getClass().getName());
//    DictionariesoxfordDAO dao = new DictionariesoxfordDAO();
//    ApiDicc api = new ApiDicc();
//
//    public ResultResponse findWord(String word) throws PreexistingEntityException, Exception {
//        log.info("Service::findWord::" + word);
//
//        DiccDef obj = api.getDefinitions(word);
//
//        if (obj.results.size() == 0) {
//            log.severe("Service::findWord::Word not found!!");
//            throw new FunctionalException("404", "Palabra no existe en diccionario");
//        }
//
//        log.info("Service::getApiResult:findDefinitions");
//        List<String> auxdef = new ArrayList<String>();
//        for(Result r:obj.results){
//            for(LexicalEntry l:r.lexicalEntries){
//                for(Entry e:l.entries){
//                    for(Sense s:e.senses){
//                        log.info("Service::Definitions::Found Definition::" + s.id);
//                        auxdef.addAll(s.definitions);
//                    }
//                }
//            }
//        }
//
//        ResultResponse response = new ResultResponse();
//
//        response.setDicPalabra(word);
//        response.setDicDefinicion(auxdef);
//
//        log.info("Service::saveDB::Definicion");
//        List<DictionariesoxfordDAO> resultSet = dao.findWord(word);
//       if (resultSet.size()==0){
//            Long dicId = dao.create(mapper(response)).getdicId();
//            response.setDicId(dicId);
//        } else {
//            response.setDicId(resultSet.get(0).getDicId());
//            dao.edit(mapper(response));
//        }
//
//        return response;
//    }
//    
//    public HistorialResponse findHistorial() {
//        List<ResultResponse> hist = new ArrayList<>();
//        dao.findDictionariesoxfordEntities().stream().forEach(def -> {
//            log.info("Procesando historial : " + def.getDicPalabra());
//            ResultResponse res = new ResultResponse();
//            res.setDicId(def.getDicId());
//            res.setDicPalabra(def.getDicPalabra());
//            res.setDicDefinicion(gson.fromJson(def.getDicDefinicion(), ArrayList.class));
//            hist.add(res);
//        });
//        log.info("Fin proceso historial...");
//        HistorialResponse response = new HistorialResponse();
//        response.getHistorial().addAll(hist);
//        return response;
//    }
//
//    public Dictionariesoxford mapper(ResultResponse r){
//        Dictionariesoxford def = new Dictionariesoxford();
//
//        def.setDicId(r.getDicId());
//        def.setDicPalabra(r.getDicPalabra());
//        def.setDicDefinicion(gson.toJson(r.getDicDefiniciones()));
//
//        return def;
//    }
}
