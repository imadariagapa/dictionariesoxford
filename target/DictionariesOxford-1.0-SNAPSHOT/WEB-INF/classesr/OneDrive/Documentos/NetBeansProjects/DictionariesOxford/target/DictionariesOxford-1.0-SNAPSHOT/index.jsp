<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="root.model.entities.Dictionariesoxford"%>
<%@page import="root.model.dao.DictionariesoxfordDAO"%>
<%@page import="root.controller.Controller"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"/>
        <link rel="stylesheet" href="css/style.css"/>
        <title>DICCIONARIO OXFORD</title>
        <c:set var="req" value="${pageContext.request}" />
        <c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />
    </head>
    <body>
                  <center><div class="alert alert-primary" role="alert">
            | INGRID MADARIAGA | SECCION 50 | TALLER DE APLICACIONES EMPRESARIALES | EVALUACION FINAL | IP CIISA |</center>
        <nav class="navbar navbar-expand-sm bg-info navbar-dark">
            <a class="navbar-brand text-white" href="#">DICCIONARIO OXFORD</a>
  
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true"></a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0" action="buscar" method="POST">
                    <input class="form-control mr-sm-4" type="text" placeholder="Palabra a buscar" name="txtSearch" aria-label="Search">
                    <button class="btn btn-outline-dark my-2 my-sm-0" type="submit" name="accion" value="Buscar">Buscar</button>
                    <button class="btn btn-outline-dark my-2 my-sm-0" type="submit" name="accion" value="Historial">Historial</button>
                </form>
            </div>
        </nav>
        <br/>
        <c:if test="${definiciones.size() > 0}">
            <div class="mx-auto" style="width: 800px;">
                <br/>
                <h1>DEFINICIONES PARA "${palabra}"</h1>
                <br/>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th><h3>RESULTADOS</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="def" items="${definiciones}">
                            <tr>
                                <th>${def}</th>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </c:if>
        <c:if test="${historial.size() > 0}">
            <div class="mx-auto" style="width: 800px;">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th colspan="3"><h3>Historial</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="h" items="${historial}">
                            <tr>
                                <th>${h.getId()}</th>
                                <th>${h.getPalabra()}</th>
                                <th>${h.getDefiniciones()}</th>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </c:if>
        
    </body>
</html>